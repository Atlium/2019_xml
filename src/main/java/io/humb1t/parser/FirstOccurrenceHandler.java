package io.humb1t.parser;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Objects;

public class FirstOccurrenceHandler extends DefaultHandler {
    private final String attributeName;
    private final String attributeValue;
    private boolean isFound = false;

    public FirstOccurrenceHandler(String attributeName, String attributeValue) {
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        for (int i = 0; i < attributes.getLength(); i++) {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            if (Objects.equals(name, attributeName) && Objects.equals(attributeValue, value)) {
                isFound = true;
                throw new SAXException("Attribute found, no need to parse anymore");
            }
        }
    }

    @Override
    public void endDocument() throws SAXException {
        if (!isFound) {
            throw new RuntimeException(String.format("Attribute with name: %s and value: %s not found", attributeName, attributeValue));
        }
    }
}
