package io.humb1t.model.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContactInfo extends AbstractJsonObject {
    private static final long serialVersionUID = -8696161315170227985L;
    @JsonProperty("ContactType")
    private String contactType;
    @JsonProperty("ContactContent")
    private String contactContent;
}
