package io.humb1t.model.contact;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import io.humb1t.model.geolocation.GeoLocation;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class PostalAddress extends AbstractJsonObject {
    private static final long serialVersionUID = -1002966379469097868L;
    @JsonProperty("AddressLine")
    private List<String> addressLines = new ArrayList<>();
    @JsonProperty("TownName")
    private String townName;
    @JsonProperty("CountrySubDivision")
    private List<String> countrySubDivisions = new ArrayList<>();
    @JsonProperty("Country")
    private String country;
    @JsonProperty("PostCode")
    private String postCode;
    @JsonProperty("GeoLocation")
    private GeoLocation geoLocation;
}
