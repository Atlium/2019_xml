package io.humb1t.model.brand;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class BrandData extends AbstractJsonObject {
    private static final long serialVersionUID = -4045841953577668771L;
    @JsonProperty("Brand")
    private List<Brand> brands = new ArrayList<>();
}
