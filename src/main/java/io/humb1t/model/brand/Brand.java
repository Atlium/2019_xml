package io.humb1t.model.brand;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class Brand extends AbstractJsonObject {
    private static final long serialVersionUID = -8348839961574804941L;
    @JsonProperty("BrandName")
    private String name;
    @JsonProperty("Branch")
    private List<Branch> branchList = new ArrayList<>();
}
