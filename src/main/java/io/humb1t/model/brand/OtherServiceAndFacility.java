package io.humb1t.model.brand;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class OtherServiceAndFacility extends AbstractJsonObject {
    private static final long serialVersionUID = -7609035014502878031L;
    @JsonProperty("Code")
    private String code;
    @JsonProperty("Name")
    private String name;
}
