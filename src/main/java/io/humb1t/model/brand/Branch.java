package io.humb1t.model.brand;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import io.humb1t.model.availability.Availability;
import io.humb1t.model.contact.ContactInfo;
import io.humb1t.model.contact.PostalAddress;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class Branch extends AbstractJsonObject {
    private static final long serialVersionUID = 5097436385130240638L;
    @JsonProperty("Identification")
    private String identification;
    @JsonProperty("SequenceNumber")
    private String sequenceNumber;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("CustomerSegment")
    private List<String> customerSegments = new ArrayList<>();
    @JsonProperty("Accessibility")
    private List<String> accessibilityList = new ArrayList<>();
    @JsonProperty("OtherServiceAndFacility")
    private List<OtherServiceAndFacility> otherServiceAndFacilityList = new ArrayList<>();
    @JsonProperty("Availability")
    private Availability availability;
    @JsonProperty("ContactInfo")
    private List<ContactInfo> contactInfoList;
    @JsonProperty("PostalAddress")
    private PostalAddress postalAddress;
}
