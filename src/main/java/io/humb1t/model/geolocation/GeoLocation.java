package io.humb1t.model.geolocation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GeoLocation extends AbstractJsonObject {
    private static final long serialVersionUID = 2700448022270694557L;
    @JsonProperty("GeographicCoordinates")
    private GeographicCoordinates geographicCoordinates;
}
