package io.humb1t.model.availability;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class OpeningHours extends AbstractJsonObject {
    private static final String DATE_FORMAT = "hh:mm:ssZ";
    private static final long serialVersionUID = 7158416026615967761L;
    @JsonProperty("OpeningTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    private Date openingTime;
    @JsonProperty("ClosingTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    private Date closingTime;

    @Override
    public String toString() {
        return "OpeningHours{" +
                "openingTime=" + new SimpleDateFormat(DATE_FORMAT).format(openingTime) +
                ", closingTime=" + new SimpleDateFormat(DATE_FORMAT).format(closingTime) +
                "} ";
    }
}
