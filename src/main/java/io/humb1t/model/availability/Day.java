package io.humb1t.model.availability;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class Day extends AbstractJsonObject {
    private static final long serialVersionUID = 5882157313912528693L;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("OpeningHours")
    private List<OpeningHours> openingHours = new ArrayList<>();
}
