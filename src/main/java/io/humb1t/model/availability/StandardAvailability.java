package io.humb1t.model.availability;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class StandardAvailability extends AbstractJsonObject {
    private static final long serialVersionUID = -7567754628838329798L;
    @JsonProperty("Day")
    private List<Day> days = new ArrayList<>();
}
