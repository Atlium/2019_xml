package io.humb1t.model.availability;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Availability extends AbstractJsonObject {
    private static final long serialVersionUID = -8850095842691050037L;
    @JsonProperty("StandardAvailability")
    private StandardAvailability standardAvailability;
}
