package io.humb1t.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.humb1t.json.AbstractJsonObject;
import io.humb1t.model.brand.BrandData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class JsonDto extends AbstractJsonObject {
    private static final long serialVersionUID = 1067392446080027396L;
    @JsonProperty("data")
    private List<BrandData> dataList = new ArrayList<>();
}
