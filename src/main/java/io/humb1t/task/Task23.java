package io.humb1t.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.humb1t.file.FileSource;
import io.humb1t.file.FileUtils;
import io.humb1t.file.factory.FileFactory;
import io.humb1t.model.JsonDto;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Task23 {
    private static final String INPUT_FILE_NAME = "example.json";
    private static final FileSource INPUT_FILE_SOURCE = FileSource.RESOURCE;
    private static final String OUTPUT_FILE_NAME = "result.json";
    private static final FileSource OUTPUT_FILE_SOURCE = FileSource.FILE_SYSTEM;

    public static void main(String[] args) {
        FileFactory fileFactory = new FileFactory();
        File inputFile = fileFactory.getFile(INPUT_FILE_SOURCE, INPUT_FILE_NAME);
        File outputFile = fileFactory.getFile(OUTPUT_FILE_SOURCE, OUTPUT_FILE_NAME);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String firstJson = FileUtils.getTextFromFile(inputFile);
            JsonDto firstJsonObject = objectMapper.readValue(firstJson, JsonDto.class);
            objectMapper.writeValue(outputFile, firstJsonObject);

            String secondJson = FileUtils.getTextFromFile(inputFile);
            JsonDto secondJsonObject = objectMapper.readValue(secondJson, JsonDto.class);

            System.out.println(String.format("First json object are %s with this object after serializing and deserializing", (Objects.equals(firstJsonObject, secondJsonObject) ? "equals" : "no equals")));
            System.out.println("First  object: " + firstJsonObject);
            System.out.println("Second object: " + secondJsonObject);
        } catch (IOException e) {
            System.out.println("Error while read/write json: " + e.getMessage());
        }
    }
}
