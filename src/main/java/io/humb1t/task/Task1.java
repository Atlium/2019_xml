package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;
import io.humb1t.json.Node;
import io.humb1t.json.NodeReader;

import javax.json.Json;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Task1 {
    private static final String FILE_NAME = "example.json";
    private static final FileSource FILE_SOURCE = FileSource.RESOURCE;
    private static final String NODE_NAME = "Brand";

    public static void main(String[] args) {
        processByReader();
    }

    private static void processByReader() {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FILE_SOURCE, FILE_NAME);
        try (JsonReader reader = Json.createReader(new FileInputStream(file))) {
            Node node = NodeReader.nextNodeWithName(NODE_NAME, reader.readObject());
            if (node == null) {
                System.out.println(String.format("Node with name '%s' not found", NODE_NAME));
            } else {
                System.out.println(String.format("Node with name %s has type %s and value: %s", node.getName(), node.getNodeType(), node.getValue()));
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
