package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class Task5 {
    private static final String FILE_NAME = "retrieveCurrentCPFFUNCP.xml";
    private static final FileSource FILE_SOURCE = FileSource.RESOURCE;

    public static void main(String[] args) {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FILE_SOURCE, FILE_NAME);
        try (FileInputStream inputStream = new FileInputStream(file)) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputStream);
            printDocument(doc, System.out);
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    public static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
        String encoding = "UTF-8";
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);

        transformer.transform(new DOMSource(doc), new StreamResult(new OutputStreamWriter(out, encoding)));
    }
}
