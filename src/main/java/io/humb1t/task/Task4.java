package io.humb1t.task;

import io.humb1t.file.FileSource;
import io.humb1t.file.factory.FileFactory;
import io.humb1t.parser.FirstOccurrenceHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.FileInputStream;

public class Task4 {
    private static final String FILE_NAME = "retrieveCurrentCPFFUNCP.xml";
    private static final FileSource FILE_SOURCE = FileSource.RESOURCE;
    private static final String ATTRIBUTE_NAME = "OBS_STATUS";
    private static final String ATTRIBUTE_VALUE = "M";

    public static void main(String[] args) {
        FileFactory fileFactory = new FileFactory();
        File file = fileFactory.getFile(FILE_SOURCE, FILE_NAME);
        try {
            ContentHandler handler = new FirstOccurrenceHandler(ATTRIBUTE_NAME, ATTRIBUTE_VALUE);
            XMLReader reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(new FileInputStream(file)));
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
