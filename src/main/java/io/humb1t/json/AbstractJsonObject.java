package io.humb1t.json;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class AbstractJsonObject implements Serializable {
    private static final long serialVersionUID = -3218674430407352226L;
    @JsonIgnore
    private transient Map<String, Object> unknownProperties = new LinkedHashMap<>();

    @JsonAnySetter
    void setUnknownProperties(String key, Object value) {
        unknownProperties.put(key, value);
    }
}
