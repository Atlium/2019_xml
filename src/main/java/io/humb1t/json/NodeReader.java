package io.humb1t.json;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.util.Map;
import java.util.Objects;

import static javax.json.JsonValue.ValueType;

public class NodeReader {
    private NodeReader() {
        //util class
    }

    public static Node nextNodeWithName(String nodeName, JsonObject object) {
        if (nodeName == null || object == null) {
            return null;
        }
        for (Map.Entry<String, JsonValue> entry : object.entrySet()) {
            Node resultNode = null;
            String key = entry.getKey();
            JsonValue value = entry.getValue();
            if (Objects.equals(key, nodeName)) {
                if (value == null) {
                    return new Node(nodeName, NodeType.VALUE, null);
                }
                return new Node(nodeName, getNodeType(value.getValueType()), value.toString());
            } else if (Objects.equals(value.getValueType(), ValueType.ARRAY)) {
                resultNode = nextArrayNodeWithName(nodeName, object.getJsonArray(key));
            } else if (Objects.equals(value.getValueType(), ValueType.OBJECT)) {
                resultNode = nextNodeWithName(nodeName, object.getJsonObject(key));
            }
            if (resultNode != null) {
                return resultNode;
            }
        }
        return null;
    }

    public static NodeType getNodeType(ValueType valueType) {
        switch (valueType) {
            case NULL:
            case NUMBER:
            case STRING:
            case TRUE:
            case FALSE:
                return NodeType.VALUE;
            case ARRAY:
                return NodeType.ARRAY;
            case OBJECT:
                return NodeType.OBJECT;
        }
        throw new IllegalArgumentException("Wrong value type: " + valueType);
    }

    public static Node nextArrayNodeWithName(String nodeName, JsonArray array) {
        for (int i = 0; i < array.size(); i++) {
            JsonValue jsonValue = array.get(i);
            if (Objects.equals(jsonValue.getValueType(), ValueType.OBJECT)) {
                return nextNodeWithName(nodeName, array.getJsonObject(i));
            } else if (Objects.equals(jsonValue.getValueType(), ValueType.ARRAY)) {
                return nextArrayNodeWithName(nodeName, array.getJsonArray(i));
            }
        }
        return null;
    }
}
