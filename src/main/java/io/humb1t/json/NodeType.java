package io.humb1t.json;

public enum NodeType {
    VALUE, OBJECT, ARRAY
}
