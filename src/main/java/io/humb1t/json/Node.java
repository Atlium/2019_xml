package io.humb1t.json;

public class Node {
    private final String name;
    private final NodeType nodeType;
    private final String value;

    public Node(String name, NodeType nodeType, String value) {
        this.name = name;
        this.nodeType = nodeType;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", nodeType=" + nodeType +
                ", value='" + value + '\'' +
                '}';
    }
}
